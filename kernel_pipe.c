
#include "tinyos.h"
#include "kernel_cc.h"
#include "kernel_dev.h"
#include "kernel_streams.h"
#include "kernel_pipe.h"
#include "util.h"


#define PIPE_SIZE 16000



/* Dummy read and write functions used in file_ops instead of NULL */
int dummyRead(void* stream, char* buffer, unsigned int size) {

	return -1;
}

int dummyWrite(void* stream, const char* buffer, unsigned int size) {

	return -1;
}


/* @pipe_read

	Reads up to 'size' bytes from stream 'stream' into buffer 'buffer'.
	If no data is available, the thread will block.

	@return: the number of bytes copied into 'buffer' in success, or -1 on error
*/
int pipe_read(void* stream, char* buffer, unsigned int size) {

	pipe_cb* pipe = stream;
	int bytes = 0; 			// The return value

	/* If there is no reader, exit */
	if( !pipe->hasReader ) return -1;

	/* If there are no data, block the thread */
	if( pipe->rbuffer->isEmpty ) {
		/* In case there is no writer, that means we have reached EOF */
		if( !pipe->hasWriter ) {
			return 0;
		} else { 
			pipe->refcount++;
			kernel_wait(&pipe->isEmpty, SCHED_USER);
			pipe->refcount++;
		}

		/* Make sure there is a reader, once again, because the status of the pipe might have changed while it was blocked */
		if( !pipe->hasReader ) return -1;

	}

		/* Read and return */
		while( bytes < size ) {
			if( ring_buf_read(pipe->rbuffer, &buffer[bytes]) ) { 		// If the read is successful
				bytes++;
			} else {
				break;
			}
		}

		// Signal the sleeping writers
		kernel_broadcast(&pipe->isFull);
		return bytes;
	
}


/* @pipe_write

	Writes up to 'size' bytes from 'buffer' to stream 'stream'.
	If the buffer is full, the thread will block

	@return: number of bytes copied from 'buffer' on success, or -1 on error
*/

int pipe_write(void* stream, const char* buffer, unsigned int size) {

	pipe_cb* pipe = stream;
	int bytes = 0;

	/* Also check if there is a reader available */
	if( !pipe->hasWriter || !pipe->hasReader ) return -1;

	/* If the buffer is full, block the thread */
	while( is_ring_buf_full(pipe->rbuffer) ) {
		pipe->refcount++;
		kernel_wait(&pipe->isFull, SCHED_USER);
		pipe->refcount--;

		/* Re-check the status of the pipe */
		if( !pipe->hasWriter || !pipe->hasReader ) return -1;

	}

	while( bytes < size ) {
		if( ring_buf_write(pipe->rbuffer, buffer[bytes]) ) {
			bytes++;
		} else {
			break;
		}
	}

	/* Signal the sleeping readers */
	kernel_broadcast(&pipe->isEmpty);
	return bytes;

}

/* @pipe_close_reader

	Close the stream object, deallocating any resources held by it 

	@return 0 on success, or -1 on error
*/

int pipe_close_reader(void* stream) {

	pipe_cb* pipe = stream;
	pipe->hasReader = 0;

	/* To deallocate the resoures held by the stream object, we make use of the reference counter we declared in pipe_cb.
		If this counter reaches 0, it means that there are no sleeping threads, therefore we can free the pipe and the buffer
	*/
	pipe->refcount--;
	if( pipe->refcount == 0 ) {
		free(pipe->rbuffer);
		free(pipe);
	} else {
		/* Signal the sleeping writers */
		kernel_broadcast(&pipe->isFull);
	}

	return 0;
}

/* @pipe_close_writer

	Same as @pipe_close_reader, but for the writer 
*/
int pipe_close_writer(void* stream) {

	pipe_cb* pipe = stream;
	pipe->hasWriter = 0;

	pipe->refcount--;
	if( pipe->refcount == 0 ) {
		free(pipe->rbuffer);
		free(pipe);
	} else {
		/* signal the sleeping readers */
		kernel_broadcast(&pipe->isEmpty);
	}

	return 0;
}

/* All the functions have been implemented so we can create the file_ops for both the writer and the reader */

file_ops writer_fops = {
	.Open = NULL,
	.Read = dummyRead,
	.Write = pipe_write,
	.Close = pipe_close_writer
};

file_ops reader_fops = {
	.Open = NULL,
	.Read = pipe_read,
	.Write = dummyWrite,
	.Close = pipe_close_reader
};


int sys_Pipe(pipe_t* pipe) {

	/* A pipe is a one-directional buffer accessed via two file ids, one for each end of the buffer (writer and reader). */

	FCB* fcb_table[2];
	Fid_t fid_table[2];

	/* To acquire the FCBs we defined above and their fids, we will use @ref FCB_reserve. 
		This function will check if there are available resources in the PCB and the FCB and
		will fill the array with the appropriate values.
	*/

	if( FCB_reserve(2, fid_table, fcb_table) ) {  		// If successful
		pipe_cb* picb = (pipe_cb *) malloc(sizeof(pipe_cb));
		init_pipe_cb(picb);

		pipe->read = fid_table[0];
		pipe->write = fid_table[1];

		fcb_table[0]->streamobj = picb;
		fcb_table[1]->streamobj = picb;

		fcb_table[0]->streamfunc = &reader_fops;
		fcb_table[1]->streamfunc = &writer_fops;

		return 0;
	} else {
		return -1;
	}
}

/* @init_pipe_cb */
void init_pipe_cb(pipe_cb* pipe) {

	pipe->rbuffer = ring_buf_init(PIPE_SIZE);
	pipe->refcount = 2;
	pipe->hasReader = 1;
	pipe->hasWriter = 1;
	pipe->isEmpty = COND_INIT;
	pipe->isFull = COND_INIT;
}