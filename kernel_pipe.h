// <Edits>

#include "tinyos.h"
#include "kernel_dev.h"
#include "kernel_streams.h"
#include "util.h"

/* Create the pipe control block, which will contain all the need information */

typedef struct pipe_control_block {

    ring_buf* rbuffer;          // The ring buffer we created in order to read/write data

    int refcount;               // Ref counter to help us know if there are threads waiting in cvs, and when we can free a pipe

    // Two "boolean" vars to show if there is a reader or a writer on a pipe
    int hasWriter;              // 1 if true, 0 otherwise
    int hasReader;              // 1 if true, 0 otherwise

    /* Variables to indicate whether the buffer is empty or full.
        We use Cond Vars because we must signal the readers in case the buffer is empty,
        or, signal the writers in case the buffer is full
    */
   CondVar isEmpty;
   CondVar isFull;

} pipe_cb;

/*  The goal is to create the file_ops functions. Below is the declarations of the file_ops functions + a function used to initialize the pipe_cb */

void init_pipe_cb(pipe_cb* pipe);
int pipe_read(void* stream, char* buffer, unsigned int size);
int pipe_write(void* stream, const char* buffer, unsigned int size);
int pipe_close_writer(void* stream);
int pipe_close_reader(void* stream);

// </Edits>