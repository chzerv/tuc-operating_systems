// <Edits>
#include "tinyos.h"
#include "kernel_dev.h"
#include "kernel_streams.h"
#include "kernel_pipe.h"
#include "util.h"

/* The socket type.
    A socket can be either:
        1. UNBOUND: the default state -thats what we get when we create a socket with @sys_Socket
        2. LISTENER: which handles requests that are made from other sockets
        3. PEER: for peer to peer connection
*/

typedef enum {
    UNBOUND,
    LISTENER,
    PEER
} socket_type;

/* Control blocks for each socket type, except UNBOUND */

typedef struct listener_socket_control_block {
    rlnode req_q;
    CondVar listener_cv;
} listener_cb;

typedef struct peer_socket_control_block {
    pipe_cb* pipe_send;
    pipe_cb* pipe_receive;
} peer_cb;

typedef struct socket_control_block {
    FCB* fcb;                // The fcb which owns this socket
    port_t port;            // The port this socket is bound to
    socket_type s_type;     // The type of the socket
    int refcount;           // Ref counter to help us know how many threads are sleeping and free the socket

    union {
        listener_cb sock_listener;
        peer_cb sock_peer;
    };

} SCB;

/* The port map */
SCB* PORT_MAP[MAX_PORT + 1];

/* The request control block */
typedef struct request_control_block {
    SCB* scb;
    rlnode req_node;            // Node for the request queue.
    int admit_flag;             // Shows if the request has been accepted (1) or not (0)
    CondVar req_cv;             // Put the thread that made the request to sleep until the request is handled
} Request;


/* Declarations : */

// Helper functions to initialize different socket types 
void initialize_SCB(SCB* scb, port_t port, FCB* fcb);
void initialize_listener_cb(listener_cb* lscb);
void initialize_peer_cb(peer_cb* prcb, pipe_cb* pipe_send, pipe_cb* pipe_receive);


// Initialize a request (sys_Connect)
Request* init_request(SCB* scb, SCB* handler);      // Add 'scb' to 'handler's' queue

// Create a peer to peer connection  (sys_Accept)
void create_p2p(SCB* scb1, SCB* scb2);
