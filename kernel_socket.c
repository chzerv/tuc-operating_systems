
#include "tinyos.h"
#include "kernel_cc.h"
#include "kernel_dev.h"
#include "kernel_streams.h"
#include "kernel_socket.h"
#include "util.h"


/* The helper functions */
void initialize_SCB(SCB* scb, port_t port, FCB* fcb) {

    scb->fcb = fcb;
    scb->port = port;
    scb->s_type = UNBOUND;
    scb->refcount = 0;
}

void initialize_listener_cb(listener_cb* lscb) {
	rlnode_init(&lscb->req_q, NULL);
	lscb->listener_cv = COND_INIT;
}

void initialize_peer_cb(peer_cb* prcb, pipe_cb* pipe_send, pipe_cb* pipe_receive) {
	prcb->pipe_receive = pipe_receive;
	prcb->pipe_send = pipe_send;
}


/* The goal is to create the file_ops for each socket type. So we need "Read, Write and Close" functions for the different socket types. */

// General
int socket_close(void* sock) {

	SCB* socket = sock;
	free(socket);
	return 0;
}

// Listener socket
int listener_socket_close(void* sock) {

	SCB* socket = sock;
	// Free the port bound to the listener socket
	PORT_MAP[socket->port] = NULL;
	// Only one listener can be bound to a single port, so, since one is closing, signal the rest
	kernel_signal(&socket->sock_listener.listener_cv);
	socket_close(socket);

	return 0;
}

// Peer socket. Read, Write and Close are needed .
int peer_socket_write(void* sock, const char* buffer, unsigned int size) {

	SCB* socket = sock;
	int bytes = -1;

	if( socket->sock_peer.pipe_send != NULL ) bytes = pipe_write(socket->sock_peer.pipe_send, buffer, size);

	return bytes;

}

int peer_socket_read(void* sock, char* buffer, unsigned int size) {

	SCB* socket = sock;
	int bytes = -1;

	if( socket->sock_peer.pipe_receive != NULL ) bytes = pipe_read(socket->sock_peer.pipe_receive, buffer, size);

	return bytes;
}

int peer_socket_close(void* sock) {

	SCB* socket = sock;

	/* If there are no sleeping threads, close the reader/writer and deallocate the resources */
	if( socket->refcount == 0 ) {
		if( socket->sock_peer.pipe_receive != NULL) pipe_close_reader(socket->sock_peer.pipe_receive);
		if( socket->sock_peer.pipe_send != NULL ) pipe_close_writer(socket->sock_peer.pipe_send);

		socket_close(socket);

		return 0;
	} else {
		return -1;
	}
}

/* File_ops */
file_ops socket_fops = {
	.Open = NULL,
	.Read = NULL,
	.Write = NULL,
	.Close = socket_close
};


file_ops listener_fops = {
	.Open = NULL,
	.Read = NULL,
	.Write = NULL,
	.Close = listener_socket_close
};


file_ops peer_fops = {
	.Open = NULL,
	.Read = peer_socket_read,
	.Write = peer_socket_write,
	.Close = peer_socket_close
};


/* Create a p2p connection */
void create_p2p(SCB* scb1, SCB* scb2) {

	/* We need to pipes to create a p2p connection */
	pipe_cb* pipe1 = (pipe_cb *) malloc(sizeof(pipe_cb));
	pipe_cb* pipe2 = (pipe_cb *) malloc(sizeof(pipe_cb));

	init_pipe_cb(pipe1);
	init_pipe_cb(pipe2);

	scb1->s_type = PEER;
	scb2->s_type = PEER;

	/* The writer of scb1 will be the reader of scb2
	   The writer of scb2 will be the reader of scb1
	*/
    initialize_peer_cb(&scb1->sock_peer, pipe1, pipe2);
	initialize_peer_cb(&scb2->sock_peer, pipe2, pipe1);

	scb1->fcb->streamfunc = &peer_fops;
	scb2->fcb->streamfunc = &peer_fops;

}

/* init a request */
Request* init_request(SCB* scb, SCB* handler) {

	Request* req = (Request *) malloc(sizeof(Request));

	req->scb = scb;
	req->admit_flag = 0;
	req->req_cv = COND_INIT;
	rlnode_init(&req->req_node, req);
	rlist_push_back(&handler->sock_listener.req_q, &req->req_node);

	return req;

}

/* sys_Socket returns a new socket bound on a port, unless the argument 'port' is NOPORT, in which case the socket won't be bound to any port.

	To implement this function, we will use the @ref FCB_reserve function, with a size of 1.

	Reasons for error:
		1. Illegal port (must check)
		2. No more available fids for the process (check done with FCB_reserve)
*/

Fid_t sys_Socket(port_t port) {

	// Illegal port
	if( (port < 1 || port > MAX_PORT) && port != NOPORT ) {
		return NOFILE;
	} else {

		FCB* fcb;
		Fid_t fid;

		if( FCB_reserve(1, &fid, &fcb) ) { 			// If successful, allocate resources

			SCB* socket = (SCB *) malloc(sizeof(SCB));

			initialize_SCB(socket, port, fcb);
			fcb->streamobj = socket;
			fcb->streamfunc = &socket_fops;

			return fid;
		}
	}
	return NOFILE;

}

/* sys_Listen initializes a socket as a listening socket.

	Notes:
		1. The socket must be bound to a port.
		2. There must only be one listening socket on a specific port.
		3. Only an UNBOUND socket can become listening

	Reasons for error:
		1. Illegal fid
		2. Socket not bound to a port
		3. Port already occupied by another listener
		4. Socket already initialized
*/

int sys_Listen(Fid_t sock) {

	/* To translate the given fid 'sock' to an FCB, we will use @ref get_fcb(), which will check if the fid is legal. If it is not, it returns NULL */
	FCB* fcb = get_fcb(sock);

	/* Checks to avoid possible errors: */

	// Illegal fid
	if( fcb != NULL && fcb->streamobj != NULL ) {
		SCB* listener = fcb->streamobj;

		// Socket not bound to a port
		if( listener != NULL && listener->port != NOPORT ) {
			SCB* socket = PORT_MAP[listener->port];

			/* Only unbound sockets can become listening && the port bound to the socket
				must not be occupied by another listener
			*/
			if( listener->s_type == UNBOUND && socket == NULL ) {

				/* Change the type to listener, and initialize the new listener socket */
				listener->s_type = LISTENER;
				initialize_listener_cb(&listener->sock_listener);
				fcb->streamfunc = &listener_fops;

				// Update the port map
				PORT_MAP[listener->port] = listener;

				return 0; 			// Success
			}
		}
	}
	return -1;

}


/* sys_Accept waits for a connection.
	This call will block waiting for a single Connect() request on the socket's port.

	Reasons for error:
		1. Illegal fid
		2. Fid not initialized by @ref Listen
		3. No more fids on the process
		4. While waiting, the listening socket 'lsock' was closed
*/

Fid_t sys_Accept(Fid_t lsock) {

	/* Translate the given fid 'lsock' to an FCB */
	FCB* fcb = get_fcb(lsock);

	/* Checks to avoid possible errors: */

	// Illegal fid
	if( fcb != NULL && fcb->streamobj != NULL ) {
		// Create the socket supposed to be the listener
		SCB* listener = fcb->streamobj;


		/* Make sure the created socket is a listener */
		if( listener->s_type == LISTENER && listener != NULL ) {

			/* @ref sys_Accept() reads requests.
				Check if the request queue is empty. If it is, that means there is
				no request, so, we put the thread to sleep until there is one.
			*/

			if( is_rlist_empty(&listener->sock_listener.req_q) ) {
				kernel_wait(&listener->sock_listener.listener_cv, SCHED_USER);
			}

			/* Create a new unbound socket for the p2p connection and make sure its valid */
			Fid_t fid2 = sys_Socket(NOPORT);
			if( fid2 == NOFILE ) {
				return -1;
			} else {
				FCB* fcb2 = get_fcb(fid2);
				SCB* connect_to = fcb2->streamobj;

				/* Get the socket which made the request */
				rlnode* req_node = rlist_pop_front(&listener->sock_listener.req_q);
				Request* req = req_node->req;
				SCB* req_socket = req->scb;

				create_p2p(req_socket, connect_to);

				/* The request has been handled so we update the flag and wakeup the thread
					which made the request
				*/
				req->admit_flag = 1;
				kernel_signal(&req->req_cv);

				return fid2; 		// Success
			}
		}
	}
	return -1; 		// Error

}

/* sys_Connect creates a connection to a listener at a specific port.

	Reasons for error:
		1. Illegal fid
		2. Illegal port
		3. No listening socket bound to the port
		4. The timeout has expired without a successful connection
*/

int sys_Connect(Fid_t sock, port_t port, timeout_t timeout) {

	/* Translate the given fid to an FCB */
	FCB* fcb = get_fcb(sock);

	/* Checks to avoid possible errors: */

	// Illegal fid
	if( fcb != NULL && fcb->streamobj != NULL ) {
		SCB* scb = fcb->streamobj;

		// Illegal port and socket not unbound
		if( (port < 1 || port > MAX_PORT) || scb == NULL || scb->s_type != UNBOUND ) {
			return -1;
		} else {
			// Create the listener socket
			SCB* listener = PORT_MAP[port];
			// If the listener socket is valid, create the request
			if( listener != NULL && listener->s_type == LISTENER ) {
				Request* req = init_request(scb, listener);

				kernel_signal(&listener->sock_listener.listener_cv);

				scb->refcount++;
				// Sleep till the request is handled
				kernel_timedwait(&req->req_cv, SCHED_USER, timeout);
				scb->refcount--;

				int reqAccepted = req->admit_flag;
				free(req);

				return (reqAccepted) ? 0 : -1;
			}
		}
	}
	return -1;

}


int sys_ShutDown(Fid_t sock, shutdown_mode how) {

	FCB* fcb = get_fcb(sock);

	if( fcb != NULL && fcb->streamobj != NULL ) {
		SCB* scb = fcb->streamobj;

		if( scb != NULL && scb->s_type == PEER ) {
			switch(how) {
				case SHUTDOWN_WRITE:
					pipe_close_writer(scb->sock_peer.pipe_send);
					scb->sock_peer.pipe_send = NULL;
					break;
				case SHUTDOWN_READ:
					pipe_close_reader(scb->sock_peer.pipe_receive);
					scb->sock_peer.pipe_receive = NULL;
					break;
				case SHUTDOWN_BOTH:
					pipe_close_reader(scb->sock_peer.pipe_receive);
					scb->sock_peer.pipe_receive = NULL;

					pipe_close_writer(scb->sock_peer.pipe_send);
					scb->sock_peer.pipe_send = NULL;
					break;
			}
			return 0;
		}
	}
	return -1;

}

