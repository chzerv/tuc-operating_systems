
#include "tinyos.h"
#include "kernel_sched.h"
#include "kernel_proc.h"
// <Edits>
#include "kernel_cc.h"

/* A function similar to @start_main_thread, but for the rest of the threads.
   The difference is that, instead of calling @Exit, it will call @ThreadExit
*/
void start_thread() {
  
  int exitval;
  TCB* tcb = CURTHREAD;
  PTCB* ptcb = tcb->owner_ptcb;
  Task call = ptcb->task;
  int argl = ptcb->argl;
  void* args = ptcb->args;
  exitval = call(argl, args);
  sys_ThreadExit(exitval);
}
/** 
  @brief Create a new thread in the current process.
  */
Tid_t sys_CreateThread(Task task, int argl, void* args)
{
	//return NOTHREAD;
  PCB* proc = CURPROC;
  PTCB* ptcb = (PTCB *) malloc(sizeof(PTCB));

  // Initialize the ptcb
  ptcb->tcb = spawn_thread(proc, start_thread, ptcb);
  ptcb->tid = (Tid_t) ptcb->tcb;
  ptcb->tcb->owner_pcb = proc;

  ptcb->isExited = 0;
  ptcb->isDetached = 0;

  ptcb->task = task;
  ptcb->argl = argl;
  ptcb->args = args;

  ptcb->ref_count = 0;
  
  ptcb->cond_v = COND_INIT;

  rlnode_init(&ptcb->ptcb_node, ptcb);
  rlist_push_back(&proc->ptcb_list, &ptcb->ptcb_node);

  proc->thread_counter++;

  wakeup(ptcb->tcb);

  return ptcb->tid;
}

/**
  @brief Return the Tid of the current thread.
 */
Tid_t sys_ThreadSelf()
{
	return (Tid_t) CURTHREAD;
}

/**
  @brief Join the given thread.
  */

 /* 1. A detached thread cannot be joined.
    2. A thread cannot join itself
*/


int sys_ThreadJoin(Tid_t tid, int* exitval) {

  PCB* proc = CURPROC;
  TCB* thread = CURTHREAD;
  rlnode* thread_list = &proc->ptcb_list;
  thread_list = thread_list->next;
  PTCB* ptcb = NULL;
  int length = rlist_len(&proc->ptcb_list);

  /* Parsing the thread list */
  for( int i = 0; i < length; i++ ) {
    ptcb = thread_list->ptcb;
    assert(ptcb != NULL);

    if( ptcb->tid == tid ) {
      if( ptcb->isDetached || thread->owner_ptcb->tid == tid ) return -1;
      else {
  
        ptcb->ref_count++;
        /* Wait on the condition variable until the thread exits. When it exits, it will signal the rest of the waiting threads */
        if( !ptcb->isExited ) kernel_wait(&ptcb->cond_v, SCHED_USER);

        if( exitval ) *exitval = ptcb->exitval;

        /* We got the exitval, so, one less thread waiting */
        ptcb->ref_count--;

        /* If there are no more threads waiting, we can clear the PTCB */
        if( ptcb->ref_count == 0 ) {
          rlist_remove(&ptcb->ptcb_node);
          free(ptcb);
        }
        return 0;
      }
    }
    thread_list = thread_list->next;
  }
  return -1;
}
      


/**
  @brief Detach the given thread.
  */
int sys_ThreadDetach(Tid_t tid)
{
	//return -1;
  PCB* proc = CURPROC;
  PTCB* ptcb = NULL;
  int length = rlist_len(&proc->ptcb_list);
  int returnThis = -1;
  
  // Save the thread list
  rlnode* thread_list = &proc->ptcb_list;
  thread_list = thread_list->next;

  for( int i = 0; i < length; i++ ) {
  
    ptcb = thread_list->ptcb;
    assert(ptcb != NULL);

    if( ptcb->tid == tid ) {
      /* Can't detach an exited thread */
      if( ptcb->isExited ) {
        returnThis = -1;
      } else {
        ptcb->isDetached = 1;
        /* Notify the rest of the threads */
        kernel_broadcast(&ptcb->cond_v);
        returnThis = 0;
      }
    }
    thread_list = thread_list->next;
  }

  return returnThis;
   
}



 /* 1. Wake up all the threads that are waiting
    2. Check if we can remove the ptcb
*/
void sys_ThreadExit(int exitval)
{

  PCB* proc = CURPROC;
  PTCB* ptcb = CURTHREAD->owner_ptcb;

  /* Wake up every thread waiting for this thread to finish. */
  kernel_broadcast(&ptcb->cond_v);

  ptcb->exitval = exitval;
  ptcb->isExited = 1;

  /* If the thread is detached, we can just remove it */
  if( ptcb->isDetached ) {
    rlist_remove(&ptcb->ptcb_node);
    free(ptcb);
  }

  proc->thread_counter--;
  kernel_sleep(EXITED, SCHED_USER);

}

